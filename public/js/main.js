$(function () {

	if (!window.location.origin) {
		window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
	}

	var model = {
		scroll: true,
		position: 0,
		localization: $('body').data('locale')
	};
	if (model.localization != 'ru') {
		model.localization = ''
	}
	else {
		$('.contacts-bg').toggleClass('_act');
	}

	var hashes = ['#title', '#comps', '#work', '#design', '#techs', '#processes', '#portfolio', '#clients', '#contacts'];

	$('.c-navicon').on('click', function () {
		var wrp_node = document.getElementsByClassName('wrp')[0];
		wrp_node.classList.toggle('_menu-open');
		this.classList.toggle('_open');
		$('.portfolio-btn').toggleClass('_shifted')
	});

	var body = document.body;
	var screens = $('.screen');
	var scrollPos = [];

	$(window).on('load', function () {
		for (var i = 0; i < screens.length; i++) {
			var offsetTop = $(screens[i]).offset().top;
			scrollPos.push(offsetTop);
		}
	});

	$('.gonext').on('click', function () {
		$('html, body').animate({
			scrollTop: $(window).height()
		}, 600)
	});

	$(window).on('scroll', function (e) {
		$('.gonext').addClass('_hidden');

		var cur_scroll_pos = $(this).scrollTop();
		if (!$('.popup').hasClass('_open')) {
			for (var i = 0; i < scrollPos.length; i++) {
				if (scrollPos[i + 1] != undefined) {
					if (cur_scroll_pos + 200 >= scrollPos[i] && cur_scroll_pos + 200 < scrollPos[i + 1]) {
						history.replaceState({}, null, hashes[i]);
						break
					}
				}
				else {

					history.replaceState({}, null, hashes[scrollPos.length - 1]);
					break
				}
			}
		}
	});

	var Popup = function () {
		var that = this,
			$popup = $('.popup');

		var events = function () {
			$popup.on('click', '.back_btn', function () {
				that.hide();
			});
			$popup.on('click', function (e) {
				if (($(e.target).closest('.project').length) || ($(e.target).closest('.project-next').length)) {
				}
				else {
					that.hide();

				}
			})
		};

		that.show = function () {
			$popup.addClass('_open');
			$('html').addClass('_overflowed')
		};

		that.hide = function () {
			$popup.removeClass('_open');
			$('.project').removeClass('_open');
			$('html').removeClass('_overflowed');
			history.pushState({}, null, window.location.origin +  "#portfolio")
			if (window.location.hash != '#portfolio') {
				$('.portfolio-btn').trigger('click');
			}
		};

		that.init = function () {
			events();
		}()
	};
	var newpopup = new Popup();

	$(window).on('popstate', function (e) {
		if (e.originalEvent.state.href) {
			if ($('.popup').hasClass('_open')) {
				$('.project-wrp').addClass('_changing');
				$('html').addClass('_overflowed');
				$('.popup').animate({
					scrollTop: $('.project-wrp').outerHeight(true)
				}, 1000, function () {
					changeProjectAjaxOnPopstate(e.originalEvent.state.href, true);
				});
			}
			else {
				var spinner = $('<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>')
				spinner.insertAfter($('.project-popup'));
				newpopup.show();
				changeProjectAjaxOnPopstate(e.originalEvent.state.href, false);
			}
		}
		else {
			var url = window.location.href;
			history.replaceState({href: ''}, null, url);
			var $popup = $('.popup');
			$popup.removeClass('_open');
			$('.project').removeClass('_open');
			$('html').removeClass('_overflowed');
			$popup.one('transitionend', function () {
				$popup.html(null);
			});
		}

		e.preventDefault();
		return false;
	});

	$('body').on('click', '.project-next', function () {
		$('html').addClass('_overflowed');

		$('.project-wrp').addClass('_changing');
		var href = $(this).attr('href');
		changeProjectAjax(href);

		return false;
	});

	$('a.portfolio-preview').on('click', function (e) {
		var href = $(this).attr('href');
		var spinner = $('<div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div>')
		spinner.insertAfter($('.project-popup'));
		newpopup.show();
		openProjectAjax(href, spinner);

		return false;
	});

	function openProjectAjax(href, spinner) {
		$('.project-popup').html(null);
		$.ajax({
			type: "GET",
			url: '/ajax' + href,
			success: function (msg) {
				var new_url = window.location.origin + '/project' + href
				history.pushState({href: href}, null, new_url);
				$('.project-popup').html(null).append(msg);
				if ($(spinner).length) {
					spinner.fadeOut(300, function () {
						$(this).remove();
					});
				}
				setTimeout(function () {
					$('.project').addClass('_open')
				}, 40);
			},
			error: function (msg) {
				console.log(msg)
			}
		});
	}

	function changeProjectAjax(href) {
		$.ajax({
			type: "GET",
			url: '/ajax' + href,
			success: function (msg) {
				var new_url = window.location.origin + '/project' + href;
				history.pushState({href: href}, null, new_url);
				var new_project = $(msg);
				$('.project-popup').append(new_project);
				setTimeout(function () {
					new_project.addClass('_opening').find('.project').addClass('_act');
					$('.project-wrp').eq(0).addClass('_hidding');
				}, 40);
				new_project.one('transitionend', function () {
					$('.project-wrp').eq(0).remove();
					new_project.removeClass('_opening');
					$('.project-popup').scrollTop(0);
				});
			},
			error: function (msg) {
				console.log(msg)
			}
		});
	}

	function changeProjectAjaxOnPopstate(href, open) {
		var url = window.location.href;
		$.ajax({
			type: "GET",
			url: '/ajax' + href,
			success: function (msg) {
				history.replaceState({href: href}, null, url);
				if (!open) {
					$('.project-popup').html(null).append(msg);

					if ($('.spinner').length) {
						$('.spinner').fadeOut(300, function () {
							$(this).remove();
						});
					}
					setTimeout(function () {
						$('.project').addClass('_open')
					}, 40);
				}
				else {
					var new_project = $(msg);
					$('.project-popup').append(new_project);
					setTimeout(function () {
						new_project.addClass('_opening').find('.project').addClass('_act');
						$('.project-wrp').eq(0).addClass('_hidding');
					}, 40);
					new_project.one('transitionend', function () {
						$('.project-wrp').eq(0).remove();
						new_project.removeClass('_opening');
						$('.project-popup').scrollTop(0);
					});
				}


			},
			error: function (msg) {
				console.log(msg)
			}
		});
	}

	function changeContactSlide() {
		var conts_bg = $('.contacts-bg'),
			conts_bg_i = $('.contacts-cont-i');
		conts_bg.toggleClass('_act');
		conts_bg_i.toggleClass('_act');
	}

	$('.contacts-tab-tx').on('click', function () {
		if (!$(this).parents('.contacts-tab').hasClass('_act')) {
			var conts_tabs = $('.contacts-tab');
			conts_tabs.toggleClass('_act');
			changeContactSlide();
		}
	});

	$('.nav-i-ln, .sidebar-logo-ln, .portfolio-btn').on('click', function (e) {
		e.preventDefault();
		var hash = $(this).attr('href');
		if (hash != window.location.hash) {
			var item = $(hash);
			$('html, body').stop(false, false).animate({
				scrollTop: item.offset().top
			}, 700, function () {
				window.location.hash = hash;
			});
		}
		return false;
	});

	$('.wrp').on('mouseenter', function (e) {
		if ($(e.relatedTarget).closest('.c-sidebar').length) {
			var wrp_node = document.getElementsByClassName('wrp')[0];
			$(wrp_node).removeClass('_menu-open');
			$('.c-navicon').removeClass('_open');
			$('.portfolio-btn').removeClass('_shifted')
		}
	});

});
