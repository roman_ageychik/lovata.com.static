var Popup = function() {
    var that = this,
        $popup = $('.popup');

    var events = function() {
        $popup.on('click', function(){
            that.hide();
        })
    };

    that.show = function () {
        $popup.addClass('_open')
    };

    that.hide = function () {
        $popup.removeClass('_open');

    };

    that.init = function(){
        events();
    }()
};

var newpopup = new Popup();


