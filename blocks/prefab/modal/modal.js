var Modal = function() {
    var that = this,
        $modalcallers = $('.modalcaller');

    var events = function() {
        $modalcallers.on('click', function(){
            var item = $(this).data('modalname');
            that.show(item);
        });
        $('.modal-close').on('click', function(){
            var item = $(this).parents('.modal');
            that.hide(item);
        });
    };
    that.show = function(item){
        $(item).addClass('_open');
        $(item).parents('.modal_holder').addClass('_open');
    };
    that.hide = function(item){
        $(item).removeClass('_open');
        $(item).parents('.modal_holder').removeClass('_open');
    };

    that.init = function(){
        events();
    }()
};

var newmodal = new Modal();


