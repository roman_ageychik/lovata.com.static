var Tabs = function () {
	var that = this;

	var events = function () {
		$('.js-tab').on('click', function () {
			var cont = $(this).data('target'),
				group = $(this).parents('.tabs').data('group'),
				tabsgroup = $('.tabs[data-group=' + group + ']'),
				contgroup = $('.tabsconts[data-group=' + group + ']')

			contgroup.find('.tabcont').removeClass('_act');
			contgroup.find(cont).addClass('_act');

			tabsgroup.find('.js-tab').removeClass('_act');
			$(this).addClass('_act');

			return false
		});
	};
	var init = function () {
		events();
	}();
};

var newtabs = new Tabs();