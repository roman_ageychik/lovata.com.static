var Slider = function(slider){
	var _self = this;
	var movingPart = slider.find('.slider-movingPart');

	_self.changeAct = function(dir){
		if(dir > 0){
			slider.find('.slider-i._act').removeClass('_act').next('.slider-i').addClass('_act');
		} else {
			slider.find('.slider-i._act').removeClass('_act').prev('.slider-i').addClass('_act');
		}
	};

	_self.events = function(){
		if ('ontouchstart' in document.documentElement) {
			slider.find('.slider-arr._left').on('touchstart', function(){
				if(!$(this).closest('.slider').find('.slider-movingPart').hasClass('_animated')) {
					var that = $(this);
					var length = that.closest('.slider').find('.slider-i').length;
					that.closest('.slider').find('.slider-i').eq(length-1).clone().prependTo(that.closest('.slider').find('.slider-movingPart'));
					that.closest('.slider').find('.slider-movingPart').css({left: -that.closest('.slider').find('.slider-i').outerWidth() + 'px'});
					setTimeout(function(){
						that.closest('.slider').find('.slider-movingPart').addClass('_animated');
						that.closest('.slider').find('.slider-movingPart').css({left: 0 + 'px'});
						setTimeout(function(){
							that.closest('.slider').find('.slider-i').eq(length).remove();
							that.closest('.slider').find('.slider-movingPart').removeClass('_animated');
						}, 600);
					}, 1);
					_self.changeAct(-1);
				}
			});
			slider.find('.slider-arr._right').on('touchstart', function(){
				var that = $(this);
				if(!that.closest('.slider').find('.slider-movingPart').hasClass('_animated')) {
					that.closest('.slider').find('.slider-i').eq(0).clone().appendTo(that.closest('.slider').find('.slider-movingPart'));
					that.closest('.slider').find('.slider-movingPart').addClass('_animated');
					that.closest('.slider').find('.slider-movingPart').css({left: -that.closest('.slider').find('.slider-i').outerWidth() + 'px'});
					setTimeout(function(){
						that.closest('.slider').find('.slider-i').eq(0).remove();
						that.closest('.slider').find('.slider-movingPart').removeClass('_animated');
						that.closest('.slider').find('.slider-movingPart').css({left: 0 + 'px'});
					}, 600);
					_self.changeAct(1);
				}
			});
		} else {
			slider.find('.slider-arr._left').on('click', function(){
				if(!$(this).closest('.slider').find('.slider-movingPart').hasClass('_animated')) {
					var that = $(this);
					var length = that.closest('.slider').find('.slider-i').length;
					that.closest('.slider').find('.slider-i').eq(length-1).clone().prependTo(that.closest('.slider').find('.slider-movingPart'));
					that.closest('.slider').find('.slider-movingPart').css({left: -that.closest('.slider').find('.slider-i').outerWidth() + 'px'});
					setTimeout(function(){
						that.closest('.slider').find('.slider-movingPart').addClass('_animated');
						that.closest('.slider').find('.slider-movingPart').css({left: 0 + 'px'});
						setTimeout(function(){
							that.closest('.slider').find('.slider-i').eq(length).remove();
							that.closest('.slider').find('.slider-movingPart').removeClass('_animated');
						}, 600);
					}, 1);
					_self.changeAct(-1);
				}
			});
			slider.find('.slider-arr._right').on('click', function(){
				var that = $(this);
				if(!that.closest('.slider').find('.slider-movingPart').hasClass('_animated')) {
					that.closest('.slider').find('.slider-i').eq(0).clone().appendTo(that.closest('.slider').find('.slider-movingPart'));
					that.closest('.slider').find('.slider-movingPart').addClass('_animated');
					that.closest('.slider').find('.slider-movingPart').css({left: -that.closest('.slider').find('.slider-i').outerWidth() + 'px'});
					setTimeout(function(){
						that.closest('.slider').find('.slider-i').eq(0).remove();
						that.closest('.slider').find('.slider-movingPart').removeClass('_animated');
						that.closest('.slider').find('.slider-movingPart').css({left: 0 + 'px'});
					}, 600);
					_self.changeAct(1);
				}
			});
		}

		$(window).on('resize', _self.moveToAct);
	};

	_self.moveToAct = function(){
		var left = -slider.find('.slider-i._act').index() * slider.find('.slider-i').outerWidth();
		movingPart.css({'transform' : 'translate3d(' + left + 'px,0,0)'});
	};

	_self.init = (function(){
		_self.moveToAct();
		_self.events();
	})();
};
$('.slider').length && $('.slider').each(function(){
	new Slider($(this));
});