(function () {
	module.exports = function (grunt) {
		var jade, javascript, name, stylus, autoprefix;
		name = 'fasthtml5me';
		stylus = {
			"styles/devStyle.css": ['styles/style.styl']
		};
		autoprefix = {
			"public/stylesheets/style.css": ["styles/devStyle.css"]
		};
		javascript = {
			"public/js/project.js": ['js/**/*.js']
		};
		grunt.initConfig({
			pkg: grunt.file.readJSON('package.json'),
			connect: {
				all: {
					options:{
						port: 9000,
						hostname: "0.0.0.0",
						middleware: function(connect, options) {
							return [
								require('grunt-contrib-livereload/lib/utils').livereloadSnippet,
								connect.static(options.base)
							];
						}
					}
				}
			},
			regarde: {
				all: {
					files:['blocks/**/*', 'views/**/*', 'data/**/*', 'styles/**/*'],
					tasks: ['stylus', 'autoprefixer', 'newer:jade', 'livereload']
				}
			},
			newer:{
				options:{
					override: function (detail, include){
						if (detail.task == 'jade'){
							include(true)
						}else{
							include(false)
						}
					}
				}
			},
			jade: {
				template: {
					data: {},
					options: {
						i18n: {
							locales: 'data/locals/ru/data.json',
							namespace: 'data',
							localeExtension: true
						},
						pretty: true
					},
					files: {
						"public/main.html": ["views/index.jade"]
					}
				}
			},
			stylus: {
                options: {
                    compress: true
                },
				compile: {
					files: stylus
				}
			},
			autoprefixer: {
				options: {
					browsers: ['last 2 versions', 'ie 8', 'ie 9']
				},
				single_file: {
					files: autoprefix
				}
			}
		});
		grunt.loadNpmTasks('grunt-contrib-jade');
		grunt.loadNpmTasks('grunt-contrib-stylus');
		grunt.loadNpmTasks('grunt-contrib-concat');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-contrib-watch');
		grunt.loadNpmTasks('grunt-contrib-connect');
		grunt.loadNpmTasks('grunt-regarde');
		grunt.loadNpmTasks('grunt-autoprefixer');
		grunt.loadNpmTasks('grunt-contrib-livereload');
		grunt.loadNpmTasks('grunt-newer');
		grunt.loadNpmTasks('grunt-jade-i18n');

		grunt.registerTask('server',[
			'livereload-start',
			'connect',
			'regarde'
		]);

		return grunt.registerTask('default', ['newer:jade', 'stylus', 'autoprefixer']);
	};

}).call(this);
