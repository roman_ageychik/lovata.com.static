var express = require('express');
var http = require('http');
var path = require('path');
var config = require('./config');
var log = require('./libs/log')(module);
var HttpError = require('./error').HttpError;
//var errorHandler = require('errorHandler');
var autoprefixer = require('express-autoprefixer');
var stylus = require('stylus');
var jade = require('jade')
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var locale = require("locale"),
	supported = ["en", "ru"];

var app = express();

app.use(locale(supported))

app.use(autoprefixer({ browsers: 'last 2 versions', cascade: false }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(require('./middleware/sendHttpError'));

var routes = require('./routes/index');

app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);


//app.use(function(err, req, res, next) {
	//if (typeof err == 'number') { // next(404);
	//	err = new HttpError(err);
	//}

	//if (err instanceof HttpError) {
	//	res.sendHttpError(err);
	//} else {
	//
	//		log.error(err);
	//		err = new HttpError(500);
	//		res.sendHttpError(err);
	//}


//});

app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


http.createServer(app).listen(process.env.PORT || config.get('port'), function(){
	log.info('Express server listening on port ' + config.get('port'));
});


module.exports = app;
